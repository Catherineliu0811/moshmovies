import _ from 'lodash';  //lodash is a package in the Javascript to deal with array conveniently.

export function paginate(items,pageNumber,pageSize){
    const startIndex=(pageNumber-1)*pageSize;
    return _(items)
    .slice(startIndex)
    .take(pageSize).
    value(); //For the current page, we want the items accordling.


}