import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash'; //lodash is used to control the utilities
//Function, string,.....Array and etc.

const Pagination =props=> {
    const{itemsCount,pageSize,onPageChange,currentPage}=props;
    console.log(currentPage);

    const pagesCount=Math.ceil(itemsCount/pageSize);

    //console.log(pagesCount);

    const pages=_.range(1,pagesCount+1)



    return(
        <nav>
            <ul className="pagination">
              {pages.map(page=>(
                <li key={page} className={page===currentPage ? 'page-item active':'page-item'}>
                    <a className="page-link" onClick={()=>onPageChange(page)}>{page}</a>
                </li>
              ))}   
            </ul>
        </nav>
    )
}

Pagination.propTypes={
  itemsCount:PropTypes.number.isRequired,
  pageSize:PropTypes.number.isRequired,
  onPageChange:PropTypes.number.isRequired,
  currentPage:PropTypes.number.isRequired

}

//The package PropTypes is to confirm if the data is valid.
 
export default Pagination;
