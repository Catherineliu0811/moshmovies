import React, { Component } from 'react';
import { FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faHeart,faHeartbeat } from '@fortawesome/free-solid-svg-icons'

class Like extends Component {

    render() { 
        let classes=faHeart;
        if(!this.props.liked) classes=faHeartbeat;
        return (<i onClick={this.props.onClick}><FontAwesomeIcon icon={ classes }></FontAwesomeIcon></i>)

        
    }
}
 
export default Like;